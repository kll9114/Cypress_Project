Feature: [DEMO test case]

 # happy path
   Scenario: I should be able to connect with a valid password and username 
        Given I open the logging page
        When I click on "Login" with the attribute "text" and the html-tag "button"
        And I type "admin" on the field "Enter Username" with the attribute "placeholder"
        And I type "admin" on the field "Enter Password" with the attribute "placeholder"
        When I click on "submit" with the attribute "type" and the html-tag "button"
        # I should pass to the next page, so the login page's element must disappear 
        Then I should not see 'Enter Username' with the attribute "placeholder"
        And I should not see 'Enter Password' with the attribute "placeholder" 

    Scenario: I should not be able to connect with empty fields
       Given I open the logging page
       When I click on "Login" with the attribute "text" and the html-tag "button"
       When I click on "submit" with the attribute "type" and the html-tag "button"
       # The page should be the same, I must not have access to the app
       Then I should see 'Enter Username' with the attribute "placeholder"
       And I should see 'Enter Password' with the attribute "placeholder"         

    Scenario: I should not be able to connect with a valid username and an empty password.
       Given I open the logging page
       When I click on "Login" with the attribute "text" and the html-tag "button"
       And I type "admin" on the field "Enter Username" with the attribute "placeholder"
       When I click on "submit" with the attribute "type" and the html-tag "button"
       # The page should be the same, I must not have access to the app
       Then I should see 'Enter Username' with the attribute "placeholder"
       And I should see 'Enter Password' with the attribute "placeholder" 
 
    Scenario: I should not be able to connect with an empty username and a valid password
       Given I open the logging page
       When I click on "Login" with the attribute "text" and the html-tag "button"
       And I type "admin" on the field "Enter Password" with the attribute "placeholder"
       When I click on "submit" with the attribute "type" and the html-tag "button"
       # The page should be the same, I must not have access to the app
       Then I should see 'Enter Username' with the attribute "placeholder"
       And I should see 'Enter Password' with the attribute "placeholder" 

    Scenario: I should be able to cancel my login 
       Given I open the logging page
        When I click on "Login" with the attribute "text" and the html-tag "button"
       When I click on "Cancel" with the attribute "text" and the html-tag "button"
       Then I should see 'Modal Login Form' with the attribute "text"

#the link "password is not functioning on my local project"
   Scenario: I should be able to ask for a new password 
       Given I open the logging page
        When I click on "Login" with the attribute "text" and the html-tag "button"
       When I click on "password?" with the attribute "text" and the html-tag "a"
        


             
