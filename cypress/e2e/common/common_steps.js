import { Given,When,Then } from '@badeball/cypress-cucumber-preprocessor';

Given (`I open the logging page`,()=>{
   cy.visit('http://localhost:8080/modal.html');
})

When (`I click on {string} with the attribute {string} and the html-tag {string}` , (selector_value, selector_type, html_tag)=> {
    if (selector_type == 'text') {
       cy.xpath("(//"+html_tag+"[contains(text(),'"+selector_value+"')])[1]").filter(":visible").click()
    }
    else 
      cy.xpath("(//"+html_tag+"[contains(@"+selector_type+", '"+selector_value+"')])[1]").filter(":visible").click()
})

Then (`I should see {string} with the attribute {string}` , (selector_value, selector_type)=> {
    if (selector_type == 'text') {
       cy.xpath("(//*[contains(text(),'"+selector_value+"')])[1]").should('be.visible')
    }
    else 
      cy.xpath("(//*[contains(@"+selector_type+", '"+selector_value+"')])[1]").should('be.visible')
})

Then (`I should not see {string} with the attribute {string}` , (selector_value, selector_type)=> {
   cy.wait(2000)
   if (selector_type == 'text') {
      cy.xpath("(//*[contains(text(),'"+selector_value+"')])[1]").should('not.be.exist')
   }
   else 
     cy.xpath("(//*[contains(@"+selector_type+", '"+selector_value+"')])[1]").should('not.be.exist')
})


When (`I type {string} on the field {string} with the attribute {string}` , (text, selector_value, selector_type)=> {
    cy.wait(1000)
    if (selector_type == 'text') {
       cy.xpath("(//*[contains(text(),'"+selector_value+"')])[1]").type(text)
    }
    else 
      cy.xpath("(//*[contains(@"+selector_type+", '"+selector_value+"')])[1]").type(text)
})

         
